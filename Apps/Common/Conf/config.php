<?php
return array(
	'URL_MODEL'=>0,         // 目前只支持 0 模式

    ///数据库配置信息开始
    'DB_TYPE'=>'mysql',
    'DB_HOST'=>'',
    'DB_NAME'=>'easyapps_cc',
    'DB_USER'=>'easyapps_cc',
    'DB_PWD' =>'easyapps_cc',
    'DB_PORT'=>'3306',
    'DB_PREFIX'=>'app_',
    ///数据库配置信息结束

    'SESSION_PREFIX'        =>  'EasyApps',
    'LAYOUT_ON'=>true,
    'DEFAULT_FILTER'        =>  '',

    'FILE_UPLOAD_API'    =>    'local', //文件存储调用的API
    'LOAD_EXT_CONFIG' => 'local,bmob,aliyun,qiniu,apicloud,leancloud', //加载扩展配置文件
    
    'TAGLIB_PRE_LOAD'=> 'Common\\TagLib\\Metronic\\Ui,Common\TagLib\Metronic\Widget',
);