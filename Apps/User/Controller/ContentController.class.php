<?php
namespace User\Controller;
use Think\Controller;
class ContentController extends CommonController {
    public function index($pageid=0,$curPage =1,$pageSize = 10){
        if (IS_POST) {
            $m = D($this->con);
            if ( method_exists( $this, '_filter' ) ) {
                $map = $this->_filter();
            }
            if ($pageid) $map['pageid'] = $pageid;
            $map['mid'] = $this->mid;
            $map['apid'] = $this->apid;
            $ret = $m->getPage($map,$curPage,(int)$pageSize);
            $data['success'] = true;
            $data['data'] = $ret['volist'];
            $data['totalRows'] = $ret['count'];
            $data['curPage'] = $curPage;
            $this->ajaxReturn($data);
        }
        unset($map);
        $Page = M('Page');
        $map['mid'] = $this->mid;
        $map['appid'] = $this->apid;
        $pages = $Page->where($map)->select();
        $this->assign('pages', $pages);
        $this->display();
    }
    public function edit($id ='')
    {
        $map['mid'] = $this->mid;
        $map['apid'] = $this->apid;
        $map['id'] = $id;
        $model = D($this->con);
        $vo = $model->getOne($map);
        $this->assign('vo', $vo);

        unset($map);
        $map['mid'] = $this->mid;
        $map['status'] = array('neq',-1);
        $Page = M('Page');
        $pages = $Page->where($map)->select();
        $this->assign('pages', $pages);
        $this->display();
    }

    public function add()
    {
        $map['mid'] = $this->mid;
        $map['status'] = array('neq',-1);
        $Page = M('Page');
        $pages = $Page->where($map)->select();
        $this->assign('pages', $pages);
        $this->display();
    }
}