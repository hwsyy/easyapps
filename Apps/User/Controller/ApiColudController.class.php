<?php
namespace User\Controller;
use Think\Controller;
class ApiColudController extends CommonController {
    public function index($t = '',$key = '')
    {

        $cfg = 'User/'.numberDir($this->apid).'config.apicould.xml';
        if (file_exists($cfg)) {
            $xml = simplexml_load_file($cfg);
            $author = $xml->author->attributes();
            $data = json_decode(json_encode($xml),TRUE);
            foreach ($data['@attributes'] as $k => $v) {
                $data[$k] = $v;
            }

            foreach ($data['preference'] as $k) {
                $data[$k['@attributes']['name']] = $k['@attributes']['value'];
            }
            $data['email'] = (string)$author['email'];
            $data['href'] = (string)$author['href'];

//             print_r($data);exit();
            $this->assign('data', $data);
        }
        $this->display();
    }

    public function downCfg()
    {
        $cfgPath = 'User/'.numberDir($this->apid);
        $cfg = $cfgPath.'config.apicould.xml';
        if (!file_exists($cfg)) {
            $this->error('文件不存在');
            exit();
        }
        $r = download_file($cfg,'config.xml');
        
    }
    
    public function style($tpl = 'themes',$theme = ''){
        $this->assign('tpl', $tpl);

        if ($theme) {
            $Apps = M('Apps');
            $map['apid'] = $this->apid;
            $map['mid'] = $this->mid;
            $data['app_apicloud_theme'] = $theme;
            $Apps->where($map)->save($data);
            exit();
        }

        $cfgPath = 'User/'.numberDir($this->apid);
        $cfg = $cfgPath.'config.apicould.xml';

        $this->display();
    }

    public function publish($t='')
    {
        if ($t == 'remote') {
            $cfgPath = 'User/'.numberDir($this->apid);
            @unlink($cfgPath.'widget.zip');
            $cfg = $cfgPath.'config.apicould.xml';
            $url = 'http://'.$_SERVER['SERVER_NAME'].'/User/ApiCloud/'.$this->ap['app_apicloud_theme'].'/index.html?apid='.$this->apid;
            $this->assign('url', $url);
            $index_str = $content = $this->fetch('remote');
            file_put_contents($cfgPath.'index.html', $index_str);
            copy($cfg, $cfgPath.'config.xml');
            $dirFiles = getfiles($cfgPath);
            $saveZip = $cfgPath.'widget.zip';
            zipApiCould($dirFiles,$saveZip);
            @unlink($cfgPath.'index.html');
            @unlink($cfgPath.'config.xml');
            $r = download_file($saveZip,'widget.zip');
            @unlink($cfgPath.'widget.zip');
        }elseif ($t == 'release') {

            $cfgPath = 'User/'.numberDir($this->apid);
            @unlink($cfgPath.'widget.zip');
            
            $cfg = $cfgPath.'config.apicould.xml';
            copy($cfg, $cfgPath.'config.xml');

            $p = 'User/ApiCloud'.$this->app['app_apicloud_theme'].'/';
            $dirFiles = getfiles($cfgPath);

            $saveZip = $cfgPath.'widget.zip';
            zipApiCould($dirFiles,$saveZip);


            $themeFiles = getfiles($p);
            zipApiCould($themeFiles,$saveZip);

            @unlink($cfgPath.'index.html');
            @unlink($cfgPath.'config.xml');

            $r = download_file($saveZip,'widget.zip');
        }
        $this->display();
    }

    public function saveCfg($key = '',$t = '')
    {
        $cfgPath = 'User/'.numberDir($this->apid);
        $cfg = $cfgPath.'config.apicould.xml';
        // print_r($_POST);
        if (!is_dir($cfgPath)) mkdir($cfgPath,0777,true);
        $this->assign('vo', $_POST);
        $cfg_str = $content = $this->fetch('cfg');
        file_put_contents($cfg, $cfg_str);
        $this->success('操作成功');
    }


}