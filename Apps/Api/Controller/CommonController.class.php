<?php
namespace Api\Controller;
use Think\Controller;
class CommonController extends Controller {
    public $ac,$md,$con;
    public $mid,$apid;
    public $user;
    public $format = "JSON";
    public function _initialize (){
        $this->ac = ACTION_NAME;
        $this->con = CONTROLLER_NAME;
        $this->md = MODULE_NAME;
        $this->apid = I('apid');
        if (I('format') == 'JSONP') {
            $this->format = 'JSONP';
        }

        if (I('callback')) {
            $this->format = 'JSONP';
        }
    }
}